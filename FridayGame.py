from random import randint
import random
import os
import schedule
import smtplib
from smtplib import SMTP
import getpass
import time
import traceback
import cPickle as pickle



# games=["ScotlandYard","Pictionary","Taboo","Jenga;"]
# players=["Varun","Nikhil","Jishin","Akhilesh","Sai","Raja","Ruhi","Kanchan","Anurag","Faraz","Karthik"]
#playerEmailDict={"Varun":"varun@creoit.com","Nikhil":"nikhil@creoit.com","Jishin":"jishin@creoit.com","Akhilesh":"akhilesh@creoit.com","Sai":"sai@creoit.com","Raja":"raja@creoit.com","Ruhi":"ruhi@creoit.com","Kanchan":"kanchan@creoit.com","Anurag":"anurag.shubham@creoit.com","Faraz":"syed@creoit.com","Karthik":"karthik@creoit.com"}
games ={}
players=[]
playerEmailDict={}
playingPlayers=[]
absentPlayers=[]

fromEmail = 'noreply.creoit@gmail.com'


def load_players():
	global players
	try:
		with open('players.p','rb') as file:
			players=pickle.load(file)
	except Exception as e:
		print traceback.format_exc(e)
		exit()


def load_games():
	global games
	try:
		with open('games.p','rb') as file:
			games=pickle.load(file)
	except Exception as e:
		print traceback.format_exc(e)
		exit()


def load_player_email_dict():
	global playerEmailDict
	try:
		with open('players_email.p','rb') as file:
			playerEmailDict=pickle.load(file)
			
	except Exception as e:
		print traceback.format_exc(e)
		exit()

def init():
	#load players
	load_players()

	#load games
	load_games()

	#load email address of individual players
	load_player_email_dict()

	sort_players()

	generate_random_game()

#schedule.every().friday.at("16:00").do(init)
schedule.every(1).minutes.do(init)

def switch(option):
	if option=='1':
		choose_Mr_X()
	elif option=='2':
		print_player_names("Pictionary")
	elif option=='3':
		print_player_names("Taboo")
	elif option=='4':
		jenga()
	else:
		invalid_option()

def jenga():
	print "Jenga"
	exit()

def invalid_option():
	print "Invalid option, your options are: ", [x for x in games];
	choose_games()

def generate_random_game():
	random_game_index= randint(1,len(games)-1)
	print "Today's game is: ",games[random_game_index-1]
	choice =str(input("Enter \n1: to proceed\n2:to choose some other game manually\n"))
	if choice=='1':
		switch(str(random_game_index))
	else:
		choose_games()

def prepare_recipients(playerList):
	recipients=[]
	for player in playerList:
		if player in playerEmailDict:
			recipients.append(playerEmailDict[player])
	return recipients

def send_email_to_everyone(game,playerList,body):
	success=False
	recipients = prepare_recipients(playerList)
	server = SMTP('smtp.gmail.com:587')
	server.ehlo()
	server.starttls()
	server.ehlo()
	while success!=True:
		password= getpass.getpass()
		try:
			server.login(fromEmail, password)
			success=True
		except:
			print "failed to login,please try again!"
	server.sendmail(fromEmail, recipients, 'Subject: %s\r\n%s' % (game, "Today's teams\n"+body))
	server.quit()
	exit()

def choose_games():
	Choice=str(input("Enter the name of the game you want to play \n 1: ScotlandYard\n 2: Pictionary\n 3:Taboo\n 4:Jenga\n"))
	switch(Choice)
	

def generate_playing_teams(game):
	random.shuffle(playingPlayers)
	team1,team2 =playingPlayers[1::2],playingPlayers[::2]
	teams=team1+team2
	niceString1=','.join(team1)
	niceString2=','.join(team2)
	niceString ="---------------------------------------------------\n"+"Team Red\n"+"["+niceString1+"]\n"+"---------------------------------------------------"+"\nTeam Yellow\n"+"["+niceString2+"]\n"+"---------------------------------------------------"
	print team1,"\n",team2
	send_email_to_everyone(game,teams,niceString)

def generate_playing_players(absentPlayerIndex):
	i=0
	absentPlayers=[]
	global playingPlayers
	absentPlayerIndex=absentPlayerIndex.split(",")
	absentPlayerIndex =map(int,sorted(set(absentPlayerIndex)))
	for i in range(0,len(players)):
		if i not in absentPlayerIndex:
			playingPlayers.append(players[i])
	

def every_one_is_playing():
	global playingPlayers
	playingPlayers =players[:]
	


def print_player_names(game):
	count=0;
	print "Enter the absent player numbers: seperated with commas or \"null\" if everyone is playing\n"
	for player in players:
		print count,":",player
		count+=1;
	absentPlayerIndex =raw_input(":")
	if absentPlayerIndex!="null":
		generate_playing_players(absentPlayerIndex)
	else:
		every_one_is_playing()
	if game!="ScotlandYard":
		generate_playing_teams(game)

def choose_Mr_X():
	print_player_names("ScotlandYard")
	randomPlayerIndex=randint(0,len(playingPlayers)-1)
	string1= "Today's Mr X is: "+playingPlayers[randomPlayerIndex]
	string2= "and "+ playingPlayers[randint(0,len(playingPlayers)-1)] +" will be assisting Mr X \""+playingPlayers[randomPlayerIndex]+ "\""
	print string1,"\n",string2
	send_email_to_everyone("ScotlandYard",playingPlayers,string1+string2)
	exit()

#def generatePLayingState():
def sort_players():
	players.sort()


if __name__=="__main__":
	# while True:
	# 	schedule.run_pending()
	# 	time.sleep(1)
	init()
	