Overview:

This is a fun project to assist with friday's game plan, to help in randomly generating teams(for Taboo and pictionary) and choosing Mr x and his/her assistant(for Scotlandyard) etc.
-------------------------------------------------------------
Language used: Python 2.7

Libraries used: 

**smtplib** : to send emails 

**getpass** : to take input as password 

**random**  : to generate random numbers 

------------------------------------------------------------

Short term Goal/s:

1. to use some kind of scheduler to execute the script(ex: **schedule** library)
2. include proper rules for all games that we have
3. make the code more readable and scalable.

------------------------------------------------------------

Long term Goal/s:

1. to implement listener module which can be run every system and take input from each player to choose the game they want to play
2. to implement guessing behaviour based on the training model and previous game's result.(little bit of machine learning).